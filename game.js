(function(){

	// GAME OBJECT
	var Game  = function (canvasId) {
		var canvas = document.getElementById(canvasId);
		this.context = canvas.getContext("2d");
		this.scoreBox = document.getElementById('score');
		this.levelBox = document.getElementById('level');

		this.winSize = {
			x: canvas.width,
			y: canvas.height
		};
		this.speed = 3;
		this.step = 10;
		this.objSize = {
			width:10,
			height:10
		};
		var startRouteMap = {
			1: 'UP',
			2: 'RIGHT',
			3: 'DOWN',
			4: 'LEFT'
		};
		this.state = 0; // 1 - snake ate herself; 2 - snake ate food; 3 - out off field
		this.score = 0;
		this.level = 1;
		this.gameOver = false;

		this.field = []; 
		for (var i = 1; i < 51; i++) {
    		this.field [i] = [];
    		for (var j = 1; j < 51; j++) {
        		this.field [i][j] = 0;
        	}
        }
        // 1 - snake, 2 - food;
        this.field[25][25] = 1; // 25:25 - first snake unit

		// start random position for food
		var rndX = 25; var rndY = 25;
		do {
			var rndX = randomInt(1, 50);
			var rndY = randomInt(1, 50);

		} while (this.field[rndX][rndY]);
		this.field[rndX][rndY] = 2;

		var startRoute = startRouteMap[randomInt(1, 4)];

		this.objects = [new Snake(this, startRoute, {x: 240, y: 240}), new Food(this, this.winSize, {x: (rndX-1) * 10, y: (rndY-1) * 10})];
		this.speed += this.level;

		var self = this;
		var run = function() {
			self.update();
			if (self.gameOver) {
				clearCtx(self.context, self.winSize);
				self.context.fillStyle="#076458";
				self.context.font="bold 60px Helvetica";
				self.context.fillText("GAME OVER", 57, 270);
				return;
			}
			self.render(self.context, self.winSize);
			setTimeout(function() {
				requestAnimationFrame(run);
			    }, 1000 / self.speed); // 1000/speed = fps
			}
		run();
	}
	Game.prototype = {
		update: function() {

			// check state
			if (this.state == 1 || this.state == 3) { // GAME OVER
				this.gameOver = true;
				return;
			}
			if (this.state == 2) {
				this.state = 0;
				this.score += 1;
				this.objects.pop();

				// create new food object
				var rndX = 0; var rndY = 0;
				do {
					var rndX = randomInt(1, 50);
					var rndY = randomInt(1, 50);

				} while (this.field[rndX][rndY]);

				this.field[rndX][rndY] = 2;
				this.objects.push(new Food(this, this.winSize, {x: (rndX-1) * 10, y: (rndY-1) * 10}));
				this.scoreBox.innerHTML = this.score;

				if (!(this.score % 4)) { // LVL UP
					this.level++;
					this.speed += this.level; 
					this.levelBox.innerHTML = this.level;
				}
				this.objects[0].appendBody();
			}

			for (var i = 0; i < this.objects.length; i++) {
				this.objects[i].update();
			}
		},
		render: function(ctx, winSize) {
			clearCtx(ctx, winSize);
			for (var i = 0; i < this.objects.length; i++) {
				if (i == 0) { // snake body
					for (var j = 0; j < this.objects[i].body.length; j++) {
						draw(ctx, this.objects[i].body[j]);
					}
					continue;
				}
				draw(ctx, this.objects[i]);
			}
		},
		checkState: function(xy, x, y) {
			if (xy < 1 || xy > 50) {
				this.state = 3;
			} else {
				this.state = this.field[x][y];
				this.field[x][y] = 1;
			}
		},
		clearFieldCell: function(x, y) {
			this.field[x][y] = 0;
		}
	}

	// SNAKE OBJECT
	var Snake  = function (game, startRoute, position) {
		this.game = game;
		this.body = [];
		this.append('head', startRoute, position);
	}
	Snake.prototype = {
		append: function(type, startRoute, position) {
			var unit = (type === 'head') ? new SnakeHead(this.game, startRoute, position) : new SnakeBody(this.game, startRoute, position);
			this.body.push(unit);
		},
		appendBody: function() {
			var lastUnit = this.body[this.body.length - 1];
			var route = lastUnit.getRoute();
			var position = lastUnit.getPosition();

			if (route === 'LEFT') position.x += this.game.objSize.width;
			if (route === 'UP') position.y += this.game.objSize.height;
			if (route === 'RIGHT') position.x -= this.game.objSize.width;
			if (route === 'DOWN') position.y -= this.game.objSize.height;

			var currX = position.x/this.game.objSize.width+1;
			var currY = position.y/this.game.objSize.height+1;

			this.game.field[currX][currY] = 1;

			this.append('body', route, position);
		},
		update: function() {
			var currRoute = '',  prevRoute = '';
			for (var i in this.body) {
  				if (this.game.state == 0 || this.game.state == 2) this.body[i].update();

  				// UPDATE BODY ROUTE
  				if (i == 0) currRoute = prevRoute = this.body[i].getRoute();
  				if (i > 0) { 
  					currRoute = this.body[i].getRoute();
  					this.body[i].setRoute(prevRoute);
  					prevRoute = currRoute;
  				}
			}
		}
	}

	
	// SNAKE BODY OBJECT
	var SnakeBody  = function (game, startRoute, position) {
		this.game = game;
		this.route = startRoute;
		this.type = 'BODY';

		this.size = {
			width: game.objSize.width,
			height: game.objSize.height
		};

		this.position = {
			x: position.x, 
			y: position.y 
		};
	}
	SnakeBody.prototype = {
		update: function() {

			var currX = this.position.x/this.game.objSize.width+1;
			var currY = this.position.y/this.game.objSize.height+1;
			var newX = currX; var newY = currY;

			this.game.clearFieldCell(currX, currY);

			// ------- move
			if(this.route == 'LEFT') {
				this.position.x -= this.game.step;
				newX -= 1;
				if (this.type == 'HEAD') this.game.checkState(newX, newX, newY);
			}
			if(this.route == 'UP') {
				this.position.y -= this.game.step;
				newY -= 1;
				if (this.type == 'HEAD') this.game.checkState(newY, newX, newY);
			}
			if(this.route == 'RIGHT') {
				this.position.x += this.game.step;
				newX += 1;
				if (this.type == 'HEAD') this.game.checkState(newX, newX, newY);
			}
			if(this.route == 'DOWN') {
				this.position.y += this.game.step;
				newY += 1;
				if (this.type == 'HEAD') this.game.checkState(newY, newX, newY);
			}
			if (this.type == 'BODY') this.game.field[newX][newY] = 1;
		},
		getRoute: function() {
			return this.route;
		},
		getPosition: function() {
			return {
				x: this.position.x,
				y: this.position.y
			};
		},
		setRoute: function(newRoute) {
			this.route = newRoute;
		}
	}

	// SNAKE HEAD OBJECT
	var SnakeHead  = function (game, startRoute, position) {

		SnakeBody.apply(this, arguments);
		this.type = 'HEAD';

		this.control = new Control();

		this.update = function() {

			var currX = this.position.x/this.game.objSize.width+1;
			var currY = this.position.y/this.game.objSize.height+1;
			var newX = currX; var newY = currY;

			// ------- control
			if (this.control.isDown(this.control.keys.LEFT) && this.route != 'LEFT' && this.route != 'RIGHT') {
				this.game.clearFieldCell(currX, currY);
				this.position.x -= this.game.step;
				newX -= 1;
				this.game.checkState(newX, newX, newY);
				this.route = 'LEFT';
				return;
			}
			if (this.control.isDown(this.control.keys.UP) && this.route != 'UP' && this.route != 'DOWN') {
				this.game.clearFieldCell(currX, currY);
				this.position.y -= this.game.step;
				newY -= 1;
				this.game.checkState(newY, newX, newY);
				this.route = 'UP';
				return;
			}
			if (this.control.isDown(this.control.keys.RIGHT) && this.route != 'RIGH' && this.route != 'LEFT') {
				this.game.clearFieldCell(currX, currY);
				this.position.x += this.game.step;
				newX += 1;
				this.game.checkState(newX, newX, newY);
				this.route = 'RIGHT';
				return;
			}
			if (this.control.isDown(this.control.keys.DOWN) && this.route != 'DOWN' && this.route != 'UP') {
				this.game.clearFieldCell(currX, currY);
				this.position.y += this.game.step;
				newY += 1;
				this.game.checkState(newY, newX, newY);
				this.route = 'DOWN';
				return;
			}
			// ------- move
			SnakeBody.prototype.update.apply(this, arguments);
		}
	}

	// inherit methods
	SnakeHead.prototype = Object.create(SnakeBody.prototype);
	SnakeHead.prototype.constructor = SnakeHead;


	// FOOD OBJECT
	var Food  = function (game, winSize, startPos) {
		this.type = 'FOOD';
		this.game = game;
		this.size = {
			width: game.objSize.width,
			height: game.objSize.height
		};
		this.position = {
			x: startPos.x,
			y: startPos.y
		};
	}
	Food.prototype = {
		update: function() {}
	}

	// KEYBOARD CONTROL OBJECT
	var Control = function() {
		var isKey = {};
		this.keys = {LEFT:37, UP:38, RIGHT:39, DOWN:40};

		window.onkeydown = function(e) {
			isKey[e.keyCode] = true;
		}
		window.onkeyup = function(e) {
			isKey[e.keyCode] = false;
		}
		this.isDown = function (keyCode) {
			return isKey[keyCode] === true;
		}

	}

	// service functions -----------
	var draw = function(ctx, obj) {
		ctx.fillStyle="#076458"; // default
		if (obj.type == 'FOOD') ctx.fillStyle="#35e5ce"; 
		ctx.fillRect(obj.position.x, obj.position.y, obj.size.width, obj.size.height);
	}
	var clearCtx = function(ctx, winSize) {
		ctx.clearRect(0, 0, winSize.x, winSize.y);
	}
	var randomInt = function (min, max) {
    	var rand = min + Math.random() * (max + 1 - min);
    	rand = Math.floor(rand);
    	return rand;
  	}
	//------------ service functions



	window.onload = function() {
		new Game("window");
	}
})();